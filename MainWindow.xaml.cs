﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Anagrafica_Studenti
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string Filevoce = "";
        public MainWindow()
        {
            InitializeComponent();
            CaricaVotiCondotta();
        }

        private void PBaggiungi_Click(object sender, RoutedEventArgs e)
        {
            Studente st = new Studente(PBCognome.Text, PBNome.Text, DPnascita.Text.ToString(), CBvotocondotta.Text,(BitmapImage)Img_studenti.Source,Filevoce);
            LVstudente.Items.Add(st);

          //  MessageBox.Show(st.ToString());
        }
        public void CaricaVotiCondotta()
        {
            for(int i=1; i < 11; i++)
            {
                CBvotocondotta.Items.Add(i);
            }
        }

        private void LVstudente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LVstudente.SelectedItem == null)
                return;
            Studente st = new Studente((Studente)LVstudente.SelectedItem);// restituisce studente 
            PBNome.Text = st.Nome;
            PBCognome.Text = st.Cognome;
            DPnascita.Text = st.Data;
            CBvotocondotta.Text = st.Voto;
            Img_studenti.Source = st.Foto;
            
        }

        private void PbCarica_Click(object sender, RoutedEventArgs e)
        {
            // indica il file da aprire 
            OpenFileDialog opendl = new OpenFileDialog();
            opendl.InitialDirectory = "c:\\";
            opendl.Filter = "jpg|*.jpg|png|*.png|img|*.img";
            opendl.Title = "Selesionare Foto";
            if (opendl.ShowDialog() == true)
            {
                Img_studenti.Source = new BitmapImage(new Uri(opendl.FileName, UriKind.Absolute));
            }
            // per salvare 
            //SaveFileDialog

        }
        private void PbVoce_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            Studente st=(Studente) button.DataContext;
            SoundPlayer sound = new SoundPlayer(st.FileVoce);

            sound.Play();
         //   MessageBox.Show(st.ToString());

        }
        //prova 1 
        private void PbVoce_Click_1(object sender, RoutedEventArgs e)
        {
            OpenFileDialog opendl = new OpenFileDialog();
            opendl.InitialDirectory = "c:\\";
            opendl.Filter = "mpg|*.*";
            opendl.Title = "Selesionare Voce studente";
            if (opendl.ShowDialog() == true)
            {
                Filevoce = opendl.FileName;
            }
        }
    }
}
