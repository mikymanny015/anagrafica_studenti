﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Anagrafica_Studenti
{
    class Studente
    {
        public string Cognome { get; set; }
        public string Nome { get; set; }
        public string Data { set; get; }
        public string Voto { set; get; }
        public BitmapImage Foto { get; set; } 
        public string FileVoce { get; set; }

        public Studente(string cognome, string nome, string data, string voto, BitmapImage i,string filevoce)
        {
            Cognome = cognome;
            Nome = nome;
            Data = data;
            Voto = voto;
            Foto = i;
            FileVoce = filevoce;
        }
        public Studente(Studente st)
        {
            Cognome = st.Cognome;
            Nome = st.Nome;
            Data = st.Data;
            Voto = st.Voto;
            Foto = st.Foto;
            FileVoce = st.FileVoce;

        }

        public override string ToString()
        {
            return Cognome + " " + Nome + " " + Data + " " + Voto;
        }
    }
}
